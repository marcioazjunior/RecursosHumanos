﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class VisualizarBeneficios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void tblVisualizar_Init(object sender, EventArgs e)
    {
        TableRow tr = new TableRow();
        TableCell tc;

        /* Cabeçalho da Tabela */
        tc = new TableCell();
        tc.Text = "<b>#</b>";
        tr.Cells.Add(tc);
        tc = new TableCell();
        tc.Text = "<b>Descrição do Benefício</b>";
        tr.Cells.Add(tc);
        tc = new TableCell();
        tc.Text = "<b>Regras do Benefício</b>";
        tr.Cells.Add(tc);
        tc = new TableCell();
        tc.Text = "<b>Valor do Benefício</b>";
        tr.Cells.Add(tc);
        tblVisualizar.Rows.Add(tr);

        Beneficio[] bens = Beneficios.GetBeneficios();

        for (int i = 0; i < bens.Length; ++i)
        {
            tr = new TableRow();

            tc = new TableCell();
            tc.Text = Convert.ToString(i + 1);
            tr.Cells.Add(tc);
            tc = new TableCell();
            tc.Text = bens[i].DescBeneficio;
            tr.Cells.Add(tc);
            tc = new TableCell();
            tc.Text = bens[i].Regras;
            tr.Cells.Add(tc);
            tc = new TableCell();
            tc.Text = "R$ " + bens[i].ValorBeneficio.ToString("0.00");
            tr.Cells.Add(tc);

            tblVisualizar.Rows.Add(tr);
        }
    }
}