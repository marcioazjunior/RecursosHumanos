﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Comum.master" AutoEventWireup="true" CodeFile="GerenciaBeneficios.aspx.cs" Inherits="GerenciaBeneficios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        a, a:visited {
            display: block;
            text-decoration: none;
            color: orange;
        }

        a:hover {
            display: block;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="filtro">
        <fieldset>
            <legend>Selecione a opção de gerência de benefícios:</legend>
            <a href="VisualizarBeneficios.aspx">Visualizar benefícios</a><br />
            <a href="CadastrarBeneficio.aspx">Cadastrar novo benefício</a><br />
            <a href="RemoverBeneficio.aspx">Deletar benefício</a><br />
        </fieldset>

        <fieldset>
            <legend>Selecione a opção de gerência de cargos:</legend>
            <a href="VisualizarCargos.aspx">Visualizar cargos</a><br />
            <a href="CadastrarCargo.aspx">Cadastrar novo cargo</a><br />
            <a href="RemoverCargo.aspx">Deletar cargo</a><br />
        </fieldset>

        <fieldset>
            <legend>Selecione a opção de gerência de benefícios para cada cargo:</legend>
            <a href="VisualizarBeneficiosCargo.aspx">Visualizar benefícios de um cargo</a><br />
            <a href="AdicionarBeneficioCargo.aspx">Adicionar benefício a um cargo</a><br />
            <a href="RemoverBeneficioCargo.aspx">Remover benefício de um cargo</a><br />
        </fieldset>
    </div>
</asp:Content>

