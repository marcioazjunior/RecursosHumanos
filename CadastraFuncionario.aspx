﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Comum.master" AutoEventWireup="true" CodeFile="CadastraFuncionario.aspx.cs" Inherits="CadastraFuncionario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="validar.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <section>
        <div class="filtro">            

           <!-- DADOS PESSOAIS-->
            <fieldset runat="server">
             <legend > Dados Pessoais</legend>
             <table>
              <tr>
               <td>
                <label for="nome">Nome: </label>
               </td>
               <td>
                <input type="text" id="txtNome" style="width: 289px" runat="server" />
               </td>

               <td>
                <label for="foto">URL foto: </label>
               </td>
               <td>
                <input type="text" id="txtUrlFoto" style="width: 289px" runat="server" />
               </td>
   
              </tr>
              <tr>
               <td class="auto-style1">
                <label>Nascimento: </label>
               </td>
               <td class="auto-style1">
               <input type="text" id="txtDia" size="2" maxlength="2" placeholder="dd" style="width: 25px" runat="server" /> 
               <input type="text" id="txtMes" size="2" maxlength="2" placeholder="mm" style="width: 25px; margin-left: 8px" runat="server" /> 
               <input type="text" id="txtAno" size="4" maxlength="4" placeholder="aaaa" style="width: 40px; margin-left: 11px" runat="server" />
               </td>
              </tr>
              <tr>
                <td class="auto-style1">
                <label for="rg">Celular: </label>
                </td>
                <td class="auto-style1">
                <input type="text" id="txtCelular" size="14" maxlength="19" style="width: 133px" runat="server" /> 
                </td>
                <td class="auto-style1">
                <label for="rg">Telefone Fixo: </label>
                </td>
                <td class="auto-style1">
                <input type="text" id="txtTelefone" size="14" maxlength="19" style="width: 133px" runat="server" /> 
                </td>
              </tr>
              <tr>
               <td class="auto-style1">
                <label for="rg">Email: </label>
               </td>
               <td class="auto-style1">
                <input type="text" id="txtEmail" size="13" maxlength="30" style="width: 133px" runat="server" /> 
               </td>
              </tr>              
             </table>
            </fieldset>

            <br />
            <!-- ENDEREÇO -->
            <fieldset>
             <legend>Dados de Endereço</legend>
             <table>
              <tr>
               <td>
                <label for="rua">Rua:</label>
               </td>
               <td class="auto-style2">
                <input type="text" id="txtRua" style="width: 323px" runat="server"  />
               </td>
               <td>
                <label for="numero">Numero:</label>
               </td>
               <td>
                <input type="text" id="txtNumeroCasa" size="4" runat="server" />
               </td>
              </tr>
              <tr>
               <td>
                <label for="bairro">Bairro: </label>
               </td>
               <td class="auto-style2">
                <input type="text" id="txtBairro" style="width: 322px" runat="server" />
               </td>
              </tr>
              <tr>
               <td class="auto-style1">
                <label for="estado">Estado:</label>
               </td>
               <td class="auto-style3">
                   <asp:DropDownList ID="lsbEstado" runat="server" DataSourceID="dsrcEstado" DataTextField="descEstado" DataValueField="descEstado" OnSelectedIndexChanged="lsbEstado_SelectedIndexChanged"></asp:DropDownList>
                   <asp:SqlDataSource ID="dsrcEstado" runat="server" ConnectionString="<%$ ConnectionStrings:ConexaoERP %>" SelectCommand="SELECT [descEstado] FROM [Estado]"></asp:SqlDataSource>
               </td>
              </tr>
              <tr>
               <td>
                <label for="cidade">Cidade: </label>
               </td>
               <td class="auto-style2">
                   <asp:DropDownList ID="lsbCidades" runat="server" OnInit="lsbCidades_Init"></asp:DropDownList>
               </td>
              </tr>              
             </table>
            </fieldset>
            <br />

            <!-- Cargo e Perfil -->
            <fieldset>
             <legend>Cargo e Perfil</legend>
             <table>
                 <tr>
                     <td>
                         <asp:DropDownList ID="lstCargos" runat="server" OnInit="lstCargos_Init"></asp:DropDownList>
                     </td>
                 </tr>
                 <tr>
                    <td>
                        <label for="bairro">Salário: </label>
                    </td>
                    <td class="auto-style2">
                        <input type="text" id="txtSalario" style="width: 322px" runat="server" />
                    </td>
                 </tr>
                 <tr>
                     <td>
                         <asp:DropDownList ID="lstPerfil" runat="server" OnInit="lstPerfil_Init"></asp:DropDownList>
                     </td>
                 </tr>
             </table>
            </fieldset>

            <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar Funcionário" OnClick="btnCadastrar_Click" />

                        </div>
                        &nbsp;
        </section>
    <!-- Não esquecer de fazer upload de imagem -->
</asp:Content>

