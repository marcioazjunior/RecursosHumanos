﻿<%@ Page Title="Recursos Humanos - Cadastrar Benefício" Language="C#" MasterPageFile="~/Comum.master" AutoEventWireup="true" CodeFile="CadastrarBeneficio.aspx.cs" Inherits="CadastrarFuncionario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="filtro" runat="server" >
        <fieldset id="fldCadastrar" runat="server" >

             <legend>Cadastro de Benefício</legend> 
            <table>  
              <tr>
               <td>
                <label for="nome">Nome (descrição) do benefício: </label>
               </td>
               <td>
                  <input type="text" id="txtDesc" maxlength="50" runat="server" /> <br />
               </td>
   
              </tr>
              <tr>
               <td class="auto-style1">
                <label>Regras do benefício: </label>
               </td>
               <td class="auto-style1">
                <input type="text" id="txtRegras" maxlength="250" runat="server" /> <br />
               </td>
              </tr>
                 <tr>
               <td class="auto-style1">
                <label for="rg">Valor do benefício:: </label>
               </td>
               <td class="auto-style1">
                 <input type="text" id="txtValor" runat="server" /> <br />
               </td>
              </tr>
            </table>
            </fieldset>
           
            
            
            <br />
            <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar Benefício" OnClick="btnCadastrar_Click" />
            <asp:Label ID="lblErro" runat="server" Visible="False"></asp:Label>
        </fieldset>
    </div>
</asp:Content>

