﻿<%@ Page Title="Recursos Humanos - Visualizar Benefícios" Language="C#" MasterPageFile="~/Comum.master" AutoEventWireup="true" CodeFile="VisualizarBeneficios.aspx.cs" Inherits="VisualizarBeneficios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        a, a:visited {
            display: block;
            text-decoration: none;
            color: orange;
        }

        a:hover {
            display: block;
            font-weight: bold;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="hor-minimalist-a">        
        <asp:Table ID="tblVisualizar" runat="server" GridLines="Both" Height="16px" HorizontalAlign="Justify" OnInit="tblVisualizar_Init" Width="100%">
        </asp:Table>        
    </div>
</asp:Content>

