﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Cargo
/// </summary>
public class Cargo
{
    private int idCargo;
    private string descCargo;

    public int IdCargo
    {
        get
        {
            return idCargo;
        }

        set
        {
            idCargo = value;
        }
    }

    public string DescCargo
    {
        get
        {
            return descCargo;
        }

        set
        {
            descCargo = value;
        }
    }
}

public class Cargos
{
    public static void CadastrarCargo(Cargo c)
    {
        string query = "INSERT INTO Cargo VALUES (@descCargo)";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@descCargo", c.DescCargo);
        cmd.ExecuteNonQuery();

        query = "SELECT idCargo FROM Cargo WHERE descCargo = @descCargo";
        cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@descCargo", c.DescCargo);
        SqlDataReader reader = cmd.ExecuteReader();

        reader.Read();
        c.IdCargo = Convert.ToInt32(reader["idCargo"]);
        reader.Close();
    }

    public static void RemoverCargo(Cargo c)
    {
        string query = "DELETE FROM Cargo WHERE idCargo=@idCargo";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@idCargo", c.IdCargo);
        
        try
        {
            cmd.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw new Exception("Impossível remover o cargo. Garanta que todos os funcionários que possuam esse cargo estejam atualizados!");
        }
    }

    public static void AtualizarDescricao(Cargo cargo, string novaDesc)
    {
        string query = "UPDATE Cargo SET descCargo=@novaDesc WHERE idCargo=@cargoID";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@cargoID", cargo.IdCargo);
        cmd.Parameters.AddWithValue("@novaDesc", novaDesc);
        cmd.ExecuteNonQuery();
    }        

    public static Cargo GetCargo(int idCargo)
    {
        string query = "SELECT * FROM Cargo WHERE idCargo=@idCargo";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@idCargo", idCargo);
        SqlDataReader reader = cmd.ExecuteReader();

        if (!reader.HasRows)
        {
            reader.Close();
            return null;
        }

        reader.Read();

        Cargo ret = new Cargo();
        ret.IdCargo = idCargo;
        ret.DescCargo = Convert.ToString(reader["descCargo"]);

        reader.Close();

        return ret;
    }

    public static Cargo[] GetCargos()
    {
        string query = "SELECT COUNT(*) as qtos FROM Cargo";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        SqlDataReader reader = cmd.ExecuteReader();

        if (!reader.HasRows)
        {
            reader.Close();
            return null;
        }

        reader.Read();
        int qtos = Convert.ToInt32(reader["qtos"]);
        reader.Close();

        if (qtos == 0)
            return null;
        
        query = "SELECT * FROM Cargo";
        cmd = new SqlCommand(query, Conexao.IniciarConexao());
        reader = cmd.ExecuteReader();

        if (!reader.HasRows)
        {
            reader.Close();
            return null;
        }

        Cargo[] ret = new Cargo[qtos];
        for (int i = 0; i < qtos; ++i)
        {
            reader.Read();
            Cargo c = new Cargo();
            c.IdCargo = Convert.ToInt32(reader["idCargo"]);
            c.DescCargo = Convert.ToString(reader["descCargo"]);
            ret[i] = c;
        }

        reader.Close();

        return ret;
    }
}
