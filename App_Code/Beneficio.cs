﻿using System;
using System.Data.SqlClient;

public class Beneficio
{
    private int idBeneficio;
    private string descBeneficio;
    private float valorBeneficio;
    private string regras;

    public int IdBeneficio
    {
        get
        {
            return idBeneficio;
        }

        set
        {
            idBeneficio = value;
        }
    }

    public string DescBeneficio
    {
        get
        {
            return descBeneficio;
        }

        set
        {
            descBeneficio = value;
        }
    }

    public float ValorBeneficio
    {
        get
        {
            return valorBeneficio;
        }

        set
        {
            valorBeneficio = value;
        }
    }

    public string Regras
    {
        get
        {
            return regras;
        }

        set
        {
            regras = value;
        }
    }
}

public class Beneficios
{    

    public static void CadastrarBeneficio(Beneficio ben)
    {
        string query = "INSERT INTO Beneficio VALUES (@descBen, @valorBen, @regrasBen)";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@descBen", ben.DescBeneficio);
        cmd.Parameters.AddWithValue("@valorBen", ben.ValorBeneficio);
        cmd.Parameters.AddWithValue("@regrasBen", ben.Regras);
        cmd.ExecuteNonQuery();
    }

    public static void RemoverBeneficio(Beneficio ben)
    {
        string query = "DELETE FROM Beneficio WHERE idBeneficio=@idBeneficio";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@idBeneficio", ben.IdBeneficio);
        cmd.ExecuteNonQuery();
    }

    public static Beneficio GetBeneficio(int idBeneficio)
    {
        string query = "SELECT * FROM Beneficio WHERE idBeneficio=@idBeneficio";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@idBeneficio", idBeneficio);
        SqlDataReader reader = cmd.ExecuteReader();

        if (!reader.HasRows)
        {
            reader.Close();
            return null;
        }

        reader.Read();
        Beneficio b = new Beneficio();
        b.IdBeneficio = Convert.ToInt32(reader["idBeneficio"]);
        b.DescBeneficio = Convert.ToString(reader["descBeneficio"]);
        b.Regras = Convert.ToString(reader["regrasBeneficio"]);
        b.ValorBeneficio = (float) Convert.ToDouble(reader["valorBeneficio"]);

        reader.Close();
        return b;
    }

    public static Beneficio[] GetBeneficios()
    {
        string query = "SELECT COUNT(*) as qtos FROM Beneficio";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        SqlDataReader reader = cmd.ExecuteReader();

        if (!reader.HasRows)
        {
            reader.Close();
            return null;
        }

        reader.Read();
        int qtos = Convert.ToInt32(reader["qtos"]);
        reader.Close();

        if (qtos == 0)
            return null;

        query = "SELECT * FROM Beneficio";
        cmd = new SqlCommand(query, Conexao.IniciarConexao());
        reader = cmd.ExecuteReader();

        if (!reader.HasRows)
        {
            reader.Close();
            return null;
        }

        Beneficio[] ret = new Beneficio[qtos];
        for (int i = 0; i < qtos; ++i)
        {
            reader.Read();
            Beneficio b = new Beneficio();
            b.IdBeneficio = Convert.ToInt32(reader["idBeneficio"]);
            b.DescBeneficio = Convert.ToString(reader["descBeneficio"]);
            b.Regras = Convert.ToString(reader["regrasBeneficio"]);
            b.ValorBeneficio = (float)Convert.ToDouble(reader["valorBeneficio"]);
            ret[i] = b;
        }

        reader.Close();

        return ret;
    }
}

