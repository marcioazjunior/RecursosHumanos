﻿using System;
using System.Data.SqlClient;

public class Funcionario
{
    private int idFuncionario;
    private string nome;
    private Cargo cargo;
    private float salario;
    private string urlFoto;
    private DateTime dataNascimento;
    private string enderecoCompleto;
    private string estado;
    private int cidade;
    private string email;
    private string celular;
    private string telefoneFixo;
    private PerfilFuncionario perfil;

    public int IdFuncionario
    {
        get
        {
            return idFuncionario;
        }

        set
        {
            idFuncionario = value;
        }
    }

    public string Nome
    {
        get
        {
            return nome;
        }

        set
        {
            nome = value;
        }
    }

    public float Salario
    {
        get
        {
            return salario;
        }

        set
        {
            salario = value;
        }
    }

    public string UrlFoto
    {
        get
        {
            return urlFoto;
        }

        set
        {
            urlFoto = value;
        }
    }

    public DateTime DataNascimento
    {
        get
        {
            return dataNascimento;
        }

        set
        {
            dataNascimento = value;
        }
    }

    public string EnderecoCompleto
    {
        get
        {
            return enderecoCompleto;
        }

        set
        {
            enderecoCompleto = value;
        }
    }

    public int Cidade
    {
        get
        {
            return cidade;
        }

        set
        {
            cidade = value;
        }
    }

    public string Email
    {
        get
        {
            return email;
        }

        set
        {
            email = value;
        }
    }

    public string Celular
    {
        get
        {
            return celular;
        }

        set
        {
            celular = value;
        }
    }

    public string TelefoneFixo
    {
        get
        {
            return telefoneFixo;
        }

        set
        {
            telefoneFixo = value;
        }
    }

    public Cargo Cargo
    {
        get
        {
            return cargo;
        }

        set
        {
            cargo = value;
        }
    }

    public PerfilFuncionario Perfil
    {
        get
        {
            return perfil;
        }

        set
        {
            perfil = value;
        }
    }
}


public class Funcionarios
{
    public static Funcionario GetFuncionario(int idFunc)
    {
        string query = "SELECT f.*, YEAR(f.dataNascFunc) as ano, MONTH(f.dataNascFunc) as mes, DAY(f.dataNascFunc) as dia FROM Funcionario f WHERE idFunc=@idFunc";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@idFunc", idFunc);
        SqlDataReader reader = cmd.ExecuteReader();
        
        if (!reader.HasRows)
        {
            reader.Close();
            return null;
        }

        reader.Read();

        Funcionario func = new Funcionario();
        func.IdFuncionario = Convert.ToInt32(reader["idFunc"]);
        func.Nome = Convert.ToString(reader["nomeFunc"]);
        func.TelefoneFixo = Convert.ToString(reader["telFixFunc"]);
        func.Salario = Convert.ToSingle(reader["salFunc"]);
        func.UrlFoto = Convert.ToString(reader["fotoFunc"]);
        func.Email = Convert.ToString(reader["emailFunc"]);
        func.EnderecoCompleto = Convert.ToString(reader["enderecoFunc"]);
        func.DataNascimento = new DateTime(Convert.ToInt32(reader["ano"]), Convert.ToInt32(reader["mes"]), Convert.ToInt32(reader["dia"]));
        func.Celular = Convert.ToString(reader["celularFunc"]);
        func.Cidade = Convert.ToInt32(reader["idCidade"]);
        func.Cargo = Cargos.GetCargo(Convert.ToInt32(reader["idCargo"]));
        func.Perfil = PerfisFuncionario.GetPerfilFuncionario(Convert.ToInt32(reader["idPerfil"]));

        reader.Close();

        return func;
    }

    public static Funcionario GetFuncionario(string nome)
    {
        string query = "SELECT idFunc FROM Funcionario WHERE nomeFunc='@nomeFunc'";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@nomeFunc", nome);
        SqlDataReader reader = cmd.ExecuteReader();

        if (!reader.HasRows)
        {
            reader.Close();
            return null;
        }

        reader.Read();

        int idFunc = Convert.ToInt32(reader["idFunc"]);

        reader.Close();

        return Funcionarios.GetFuncionario(idFunc);
    }

    public static Funcionario[] GetFuncionarios()
    {
        string query = "SELECT COUNT(*) as qtos FROM Funcionario";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        SqlDataReader reader = cmd.ExecuteReader();

        reader.Read();

        int qtos = Convert.ToInt32(reader["qtos"]);
        
        reader.Close();

        if (qtos == 0)
            return null;

        query = "SELECT idFunc FROM Funcionario";
        cmd = new SqlCommand(query, Conexao.IniciarConexao());
        reader = cmd.ExecuteReader();
        
        int[] vet = new int[qtos];
        for (int i = 0; i < qtos; ++i)
        {
            reader.Read();
            vet[i] = Convert.ToInt32(reader["idFunc"]);
        }

        reader.Close();

        Funcionario[] ret = new Funcionario[qtos];

        for (int i = 0; i < qtos; ++i)        
            ret[i] = Funcionarios.GetFuncionario(vet[i]);

        return ret;
    }

    public static void RegistrarFuncionario(Funcionario fun)
    {
        string query = "INSERT INTO Funcionario VALUES(@idCargo, @nomeFunc, @salFunc, @fotoFunc, @dataNasc, @endereco, @cidade, @email, @celular, @telefone, @perfil)";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@idCargo", fun.Cargo.IdCargo);
        cmd.Parameters.AddWithValue("@nomeFunc", fun.Nome);
        cmd.Parameters.AddWithValue("@salFunc", fun.Salario);
        cmd.Parameters.AddWithValue("@fotoFunc", fun.Cargo.IdCargo);
        cmd.Parameters.AddWithValue("@dataNasc", fun.DataNascimento.ToShortDateString());
        cmd.Parameters.AddWithValue("@endereco", fun.EnderecoCompleto);
        cmd.Parameters.AddWithValue("@cidade", fun.Cidade);
        cmd.Parameters.AddWithValue("@email", fun.Email);
        cmd.Parameters.AddWithValue("@celular", fun.Celular);
        cmd.Parameters.AddWithValue("@telefone", fun.TelefoneFixo);
        cmd.Parameters.AddWithValue("@perfil", fun.Perfil.IdPerfilFuncinario);
        cmd.ExecuteNonQuery();
    }

    public static void DemitirFuncionario(Funcionario fun)
    {
        //TODO: adicionar campo emergencial para verificar se um funcionário foi demitido
    }
}

