﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Design_OFICIAL.Startup))]
namespace Design_OFICIAL
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
