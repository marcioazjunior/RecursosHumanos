﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PerfilFuncionario
/// </summary>
public class PerfilFuncionario
{
    private int idPerfilFuncinario;
    public int IdPerfilFuncinario
    {
        get { return idPerfilFuncinario; }
        set { idPerfilFuncinario = value; }
    }

    private string descPerfilFuncionario;
    public string DescPerfilFuncionario
    {
        get { return descPerfilFuncionario; }
        set { descPerfilFuncionario = value; }
    }
}

public class PerfisFuncionario
{
    public static void CadastrarPerfil(PerfilFuncionario pf)
    {
        if (pf == null)
            return;

        string query = "INSERT INTO PerfilFuncionario VALUES (@descPerfil)";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@descPerfil", pf.DescPerfilFuncionario);
        cmd.ExecuteNonQuery();
    }

    public static void RemoverPerfil(PerfilFuncionario pf)
    {
        string query = "DELETE FROM PerfilFuncionario WHERE idPerfilFuncionario=@idPerfil";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@idPerfil", pf.IdPerfilFuncinario);

        try
        {
            cmd.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw new Exception("Impossível remover o perfil. Tenha certeza que nenhum funcionário possui este perfil.");
        }
    }

    public static PerfilFuncionario GetPerfilFuncionario(int idPerfil)
    {
        string query = "SELECT * FROM PerfilFuncionario WHERE idPerfil=@idPerfil";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@idPerfil", idPerfil);
        SqlDataReader reader = cmd.ExecuteReader();

        if (!reader.HasRows)
        {
            reader.Close();
            return null;
        }

        reader.Read();

        PerfilFuncionario ret = new PerfilFuncionario();
        ret.IdPerfilFuncinario = Convert.ToInt32(reader["idPerfil"]);
        ret.DescPerfilFuncionario = Convert.ToString(reader["descPerfil"]);        

        reader.Close();

        return ret;
    }

    public static PerfilFuncionario[] GetPerfisFuncionario()
    {
        string query = "SELECT COUNT(*) as qtos FROM PerfilFuncionario";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        SqlDataReader reader = cmd.ExecuteReader();

        reader.Read();
        int qtos = Convert.ToInt32(reader["qtos"]);
        reader.Close();

        if (qtos == 0)
            return null;

        query = "SELECT * FROM PerfilFuncionario";
        cmd = new SqlCommand(query, Conexao.IniciarConexao());
        reader = cmd.ExecuteReader();

        PerfilFuncionario[] ret = new PerfilFuncionario[qtos];

        for (int i = 0; i < qtos; ++i)
        {
            reader.Read();

            PerfilFuncionario toAdd = new PerfilFuncionario();

            toAdd.IdPerfilFuncinario = Convert.ToInt32(reader["idPerfil"]);
            toAdd.DescPerfilFuncionario = Convert.ToString(reader["descPerfil"]);

            ret[i] = toAdd;
        }

        reader.Close();

        return ret;
    }
}