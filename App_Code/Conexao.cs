﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Conexao
/// </summary>
public class Conexao
{
    public static SqlConnection IniciarConexao()
    {
        string strConnection = ConfigurationManager.ConnectionStrings["ConexaoERP"].ConnectionString;
        SqlConnection sqlConnection = new SqlConnection(strConnection);

        if (sqlConnection.State == ConnectionState.Closed)
        {
            //Abre a conexão.
            sqlConnection.Open();
        }

        return sqlConnection;
    
    }
}