﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CadastraFuncionario : System.Web.UI.Page
{
    private Cargo[] cargos = Cargos.GetCargos();
    private PerfilFuncionario[] perfis = PerfisFuncionario.GetPerfisFuncionario();

    private int getIdCidade(string cidade)
    {
        string query = "SELECT idCidade FROM Cidade WHERE descCidade=@descCidade";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@descCidade", cidade);
        SqlDataReader reader = cmd.ExecuteReader();

        if (!reader.HasRows)
        {
            reader.Close();
            return -1;
        }

        reader.Read();
        int ret = Convert.ToInt32(reader["idCidade"]);
        reader.Close();

        return ret;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        cargos = Cargos.GetCargos();
    }

    private void AtualizaCidades()
    {
        string query = "SELECT * FROM Cidade WHERE idEstado = (SELECT idEstado FROM Estado WHERE descEstado='@estado')";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        cmd.Parameters.AddWithValue("@estado", lsbCidades.SelectedValue);
        SqlDataReader reader = cmd.ExecuteReader();

        if (!reader.HasRows)
        {
            reader.Close();
            return;
        }

        while (reader.Read())
        {
            ListItem item = new ListItem(Convert.ToString(reader["descCidade"]));
            lsbCidades.Items.Add(item);
        }

        reader.Close();
    }

    protected void lsbEstado_SelectedIndexChanged(object sender, EventArgs e)
    {
        AtualizaCidades();
    }

    protected void lsbCidades_Init(object sender, EventArgs e)
    {
        string query = "SELECT * FROM Cidade WHERE idEstado = (SELECT TOP 1 idEstado FROM Estado)";
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        SqlDataReader reader = cmd.ExecuteReader();

        if (!reader.HasRows)
        {
            reader.Close();
            return;
        }

        while (reader.Read())
        {
            ListItem item = new ListItem(Convert.ToString(reader["descCidade"]));
            lsbCidades.Items.Add(item);
        }

        reader.Close();
    }

    protected void btnCadastrar_Click(object sender, EventArgs e)
    {
        Funcionario f = new Funcionario();

        f.Nome = txtNome.Value;
        f.Cargo = cargos[lstCargos.SelectedIndex];
        f.Perfil = perfis[lstPerfil.SelectedIndex];
        f.Cidade = this.getIdCidade(lsbCidades.SelectedValue);
        f.EnderecoCompleto = txtRua.Value + ", " + txtNumeroCasa.Value + " - " + txtBairro.Value;
        f.Email = txtEmail.Value;
        f.DataNascimento = new DateTime(Convert.ToInt32(txtAno.Value), Convert.ToInt32(txtMes.Value), Convert.ToInt32(txtDia.Value));
        f.Celular = txtCelular.Value;
        f.TelefoneFixo = txtTelefone.Value;
        f.Salario = Convert.ToSingle(txtSalario.Value);
        f.UrlFoto = txtUrlFoto.Value;

        Funcionarios.RegistrarFuncionario(f);
    }

    protected void lstCargos_Init(object sender, EventArgs e)
    {
        if (cargos == null)
            return;

        for (int i = 0; i < cargos.Length; ++i)
        {
            ListItem li = new ListItem(cargos[i].DescCargo);
            this.lstCargos.Items.Add(li);
        }
    }

    protected void lstPerfil_Init(object sender, EventArgs e)
    {
        if (perfis == null)
            return;

        for (int i = 0; i < perfis.Length; ++i)
        {
            ListItem li = new ListItem(perfis[i].DescPerfilFuncionario);
            this.lstPerfil.Items.Add(li);
        }
    }
}