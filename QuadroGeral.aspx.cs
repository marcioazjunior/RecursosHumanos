﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class QuadroGeral : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void tblFuncionarios_Init(object sender, EventArgs e)
    {
        TableCell tc;
        TableRow tr;

        tr = new TableHeaderRow();

        tc = new TableHeaderCell();
        tc.Text = "Nome do Funcionário";
        tr.Cells.Add(tc);
        tc = new TableHeaderCell();
        tc.Text = "Cargo do Funcionário";
        tr.Cells.Add(tc);
        tc = new TableHeaderCell();
        tc.Text = "Salário do Funcionário";
        tr.Cells.Add(tc);
        tc = new TableHeaderCell();
        tc.Text = "";
        tr.Cells.Add(tc);

        tblFuncionarios.Rows.Add(tr);

        Funcionario[] funcs = Funcionarios.GetFuncionarios();

        for (int i = 0; i < funcs.Length; ++i)
        {
            tr = new TableRow();

            tc = new TableCell();
            tc.Text = funcs[i].Nome;
            tr.Cells.Add(tc);
            tc = new TableCell();
            tc.Text = funcs[i].Cargo.DescCargo;
            tr.Cells.Add(tc);
            tc = new TableCell();
            tc.Text = Convert.ToString(funcs[i].Salario);
            tr.Cells.Add(tc);
            tc = new TableCell();
            tc.Text = "<a href=\"VisualizarFuncionario.aspx?IdFuncionario=" + funcs[i].IdFuncionario + "\" target=\"_blank\">Mais informações</a>";
            tr.Cells.Add(tc);

            tblFuncionarios.Rows.Add(tr);
        }
    }
}