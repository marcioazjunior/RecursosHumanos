﻿<%@ Page Title="Recursos Humanos - Cadastrar Cargo" Language="C#" MasterPageFile="~/Comum.master" AutoEventWireup="true" CodeFile="CadastrarCargo.aspx.cs" Inherits="CadastrarCargo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="filtro">

        <fieldset>
            <legend>Cadastro de Benefício</legend>
            <table>           
              <tr>
               <td>
                <label for="nome">Nome (descrição) do benefício: </label>
               </td>
               <td>
                  Nome do cargo: <input type="text" id="txtNomeCargo" runat="server" /> <br />
               </td>
   
              </tr>
              <tr>
               <td class="auto-style1">
                <label>Benefícios do cargo: </label>
               </td>
               <td class="auto-style1">
                <asp:Table ID="tblBeneficios" runat="server" OnInit="tblBeneficios_Init"></asp:Table>
               </td>
              </tr>      
              </table>     


            <asp:Button ID="btnCadastrarCargo" runat="server" Text="Cadastrar Cargo" OnClick="btnCadastrarCargo_Click" />
        </fieldset>
    </div>
</asp:Content>

