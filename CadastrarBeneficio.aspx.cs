﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CadastrarFuncionario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnCadastrar_Click(object sender, EventArgs e)
    {
        string desc = txtDesc.Value;
        string regras = txtRegras.Value;

        if (!(0 < desc.Length && desc.Length <= 50 && 0 < desc.Length && desc.Length <= 250))
        {
            lblErro.Visible = true;
            lblErro.Text = "Preencha todos os campos.";
            return;
        }

        float val = 0.00f;
        try
        {
            val = Convert.ToSingle(txtValor.Value, CultureInfo.InvariantCulture.NumberFormat);
        }
        catch
        {
            lblErro.Visible = true;
            lblErro.Text = "Digite o valor do benefício corretamente.";
            return;
        }

        Beneficio ben = new Beneficio();
        ben.DescBeneficio = desc;
        ben.Regras = regras;
        ben.ValorBeneficio = val;

        Beneficios.CadastrarBeneficio(ben);
    }
}