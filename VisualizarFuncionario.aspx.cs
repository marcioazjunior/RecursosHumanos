﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class VisualizarFuncionario : System.Web.UI.Page
{
    private string getCidade(int idCidade)
    {
        string query = "SELECT descCidade FROM Cidade WHERE idCidade=" + idCidade;
        SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
        SqlDataReader reader = cmd.ExecuteReader();

        if (!reader.HasRows)
        {
            reader.Close();
            return null;
        }

        reader.Read();

        string cidade = Convert.ToString(reader["descCidade"]);

        reader.Close();

        return cidade;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void tblFuncionario_Init(object sender, EventArgs e)
    {
        int idFuncionario = Convert.ToInt32(this.Request.QueryString["IdFuncionario"]);
        Funcionario func = Funcionarios.GetFuncionario(idFuncionario);

        if (func == null)
        {
            lblErro.Visible = true;
            lblErro.Text = "Impossível obter informação do funcionário solicitado.";
            return;
        }

        TableRow tr;
        TableCell tc;

        tr = new TableRow();

        tc = new TableCell();
        tc.Text = func.Nome;
        
        tc.Text = "<img width=60px height=80px src=\"" + func.UrlFoto + "\" /> <br /> " + func.Nome;
        tr.Cells.Add(tc);

        tblFuncionario.Rows.Add(tr);

        tr = new TableRow();

        tc = new TableCell();
        tc.Text = "Cargo do Funcionário:";
        tr.Cells.Add(tc);

        tc = new TableCell();
        tc.Text = func.Cargo.DescCargo;
        tr.Cells.Add(tc);

        tblFuncionario.Rows.Add(tr);

        tr = new TableRow();

        tc = new TableCell();
        tc.Text = "Email:";
        tr.Cells.Add(tc);

        tc = new TableCell();
        tc.Text = func.Email;
        tr.Cells.Add(tc);

        tblFuncionario.Rows.Add(tr);

        tr = new TableRow();

        tc = new TableCell();
        tc.Text = "Salário do Funcionário:";
        tr.Cells.Add(tc);

        tc = new TableCell();
        tc.Text = Convert.ToString(func.Salario);
        tr.Cells.Add(tc);

        tblFuncionario.Rows.Add(tr);

        tr = new TableRow();

        tc = new TableCell();
        tc.Text = "Data de Nascimento:";
        tr.Cells.Add(tc);

        tc = new TableCell();
        tc.Text = func.DataNascimento.ToShortDateString();
        tr.Cells.Add(tc);

        tblFuncionario.Rows.Add(tr);

        tr = new TableRow();

        tc = new TableCell();
        tc.Text = "Cidade:";
        tr.Cells.Add(tc);

        tc = new TableCell();
        tc.Text = this.getCidade(func.Cidade);
        tr.Cells.Add(tc);

        tblFuncionario.Rows.Add(tr);

        tr = new TableRow();

        tc = new TableCell();
        tc.Text = "Endereço:";
        tr.Cells.Add(tc);

        tc = new TableCell();
        tc.Text = func.EnderecoCompleto;
        tr.Cells.Add(tc);

        tblFuncionario.Rows.Add(tr);

        tr = new TableRow();

        tc = new TableCell();
        tc.Text = "Telefone Fixo:";
        tr.Cells.Add(tc);

        tc = new TableCell();
        tc.Text = func.TelefoneFixo;
        tr.Cells.Add(tc);

        tblFuncionario.Rows.Add(tr);

        tr = new TableRow();

        tc = new TableCell();
        tc.Text = "Celular:";
        tr.Cells.Add(tc);

        tc = new TableCell();
        tc.Text = func.Celular;
        tr.Cells.Add(tc);

        tblFuncionario.Rows.Add(tr);
    }

    protected void tblFuncionario_Init2(object sender, EventArgs e)
    {
        int idFuncionario = Convert.ToInt32(this.Request.QueryString["IdFuncionario"]);
        Funcionario func = Funcionarios.GetFuncionario(idFuncionario);

        if (func == null)
        {
            lblErro.Visible = true;
            lblErro.Text = "Impossível obter informação do funcionário solicitado.";
            return;
        }

        TableRow tr;
        TableCell tc;
        TextBox tb;
        DropDownList lst;

        /* Nome do Funcionário */
        tr = new TableRow();

        tc = new TableCell();
        tc.Text = "Nome do Funcionário:";
        tr.Cells.Add(tc);

        tc = new TableCell();
        tb = new TextBox();
        tb.ID = "txtNome";
        tb.Text = func.Nome;
        tc.Controls.Add(tb);
        tr.Cells.Add(tc);
        /* Nome do Funcionário */

        /* Cargo do Funcionário */
        tr = new TableRow();

        tc = new TableCell();
        tc.Text = "Cargo do Funcionário:";
        tr.Cells.Add(tc);

        tc = new TableCell();
        lst = new DropDownList();

        tb.ID = "";
        tb.Text = func.Nome;
        tc.Controls.Add(tb);
        tr.Cells.Add(tc);
        /* Cargo do Funcionário */

        //func.Perfil, Salario, TelefoneFixo, func.Cargo, Celular, DataNascimento, func.Cidade, Email, EnderecoCompleto

        tc = new TableCell();
        tc.Text = "Cargo do Funcionário:";
        tr.Cells.Add(tc);
        tc = new TableCell();
        tc.Text = "Nome do Funcionário:";
        tr.Cells.Add(tc);

        tblFuncionario.Rows.Add(tr);
        
    }
}