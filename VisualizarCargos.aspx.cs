﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class VisualizarCargo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void tblVisualizar_Init(object sender, EventArgs e)
    {
        TableRow tr = new TableRow();
        TableCell tc;

        /* Cabeçalho da Tabela */
        tc = new TableCell();
        tc.Text = "<b>#</b>";
        tr.Cells.Add(tc);
        tc = new TableCell();
        tc.Text = "<b>Nome do Cargo</b>";
        tr.Cells.Add(tc);
        tc = new TableCell();
        tc.Text = "<b>Benefícios do Cargo</b>";
        tr.Cells.Add(tc);
        tblVisualizar.Rows.Add(tr);

        Cargo[] cargos = Cargos.GetCargos();
        
        for (int i = 0; i < cargos.Length; ++i)
        {
            tr = new TableRow();

            tc = new TableCell();
            tc.Text = Convert.ToString(i + 1);
            tr.Cells.Add(tc);
            tc = new TableCell();
            tc.Text = cargos[i].DescCargo;
            tr.Cells.Add(tc);

            tc = new TableCell();

            string query = "SELECT b.descBeneficio as nomeBen FROM Beneficio b INNER JOIN CargoBeneficio bc ON bc.idBeneficio=b.idBeneficio WHERE bc.idCargo=" + cargos[i].IdCargo;
            SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
            SqlDataReader reader = cmd.ExecuteReader();

            bool first = true;
            string final = "";
            while (reader.Read())
            {
                string nomeBen = Convert.ToString(reader["nomeBen"]);

                if (!first)
                    final += ", " + nomeBen;
                else
                    final += nomeBen;

                first = false;
            }
            tc.Text = final;

            reader.Close();

            tr.Cells.Add(tc);

            tblVisualizar.Rows.Add(tr);
        }
    }
}