﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RemoverBeneficio : System.Web.UI.Page
{
    private Beneficio[] bens;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lstBeneficios_Init(object sender, EventArgs e)
    {
        this.bens = Beneficios.GetBeneficios();

        if (this.bens == null)
            return;

        lstBeneficios.Items.Clear();

        for (int i = 0; i < bens.Length; ++i)
        {
            ListItem item = new ListItem(bens[i].DescBeneficio);
            lstBeneficios.Items.Add(item);
        }
    }

    protected void btnRemover_Click(object sender, EventArgs e)
    {
        int to_remove_idx = lstBeneficios.SelectedIndex;
        Beneficio to_remove = bens[to_remove_idx];
        
        try
        {
            Beneficios.RemoverBeneficio(to_remove);
            Response.Redirect("RemoverBeneficio.aspx"); /* Faz refresh na página */
        }
        catch (Exception)
        {
            lblErro.Visible = true;
            lblErro.Text = "Não foi possível remover o benefício selecionado. Certifique-se que nenhum funcionário possui o benefício informado!";
        }
    }
}