﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CadastrarCargo : System.Web.UI.Page
{
    private CheckBox[] beneficiosChk;
    private Beneficio[] bens = Beneficios.GetBeneficios();

    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void tblBeneficios_Init(object sender, EventArgs e)
    {
        TableRow tr;
        TableCell tc;        
        
        beneficiosChk = new CheckBox[bens.Length];

        for (int i = 0; i < bens.Length; ++i)
        {
            tr = new TableRow();            

            tc = new TableCell();
            tc.Text = bens[i].DescBeneficio;
            tr.Cells.Add(tc);

            tc = new TableCell();
            beneficiosChk[i] = new CheckBox();
            beneficiosChk[i].Enabled = beneficiosChk[i].Visible = true;
            tc.Controls.Add(beneficiosChk[i]);         
            tr.Cells.Add(tc);

            tblBeneficios.Rows.Add(tr);
        }
    }

    protected void btnCadastrarCargo_Click(object sender, EventArgs e)
    {
        string descCargo = txtNomeCargo.Value;

        Cargo c = new Cargo();
        c.DescCargo = descCargo;

        Cargos.CadastrarCargo(c);

        for (int i = 0; i < beneficiosChk.Length; ++i)
        {
            if (beneficiosChk[i].Checked == false)
                continue;

            string query = "INSERT INTO CargoBeneficio VALUES (@idCargo, @idBeneficio)";
            SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
            cmd.Parameters.AddWithValue("@idCargo", c.IdCargo);
            cmd.Parameters.AddWithValue("@idBeneficio", bens[i].IdBeneficio);
            cmd.ExecuteNonQuery();
        }
    }
}