﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Comum.master" AutoEventWireup="true" CodeFile="RemoverBeneficio.aspx.cs" Inherits="RemoverBeneficio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="filtro">
        <fieldset>
            <asp:DropDownList ID="lstBeneficios" runat="server" OnInit="lstBeneficios_Init"></asp:DropDownList>
            <asp:Button ID="btnRemover" runat="server" Text="Remover" OnClick="btnRemover_Click" />
        </fieldset>
        <asp:Label ID="lblErro" runat="server" Visible="False"></asp:Label>
    </div>
</asp:Content>

