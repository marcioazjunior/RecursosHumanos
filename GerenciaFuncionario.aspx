﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Comum.master" AutoEventWireup="true" CodeFile="GerenciaFuncionario.aspx.cs" Inherits="GerenciaFuncionario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <style>
        a, a:visited {
            display: block;
            text-decoration: none;
            color: orange;
        }

        a:hover {
            display: block;
            font-weight: bold;
        }
    </style>
    <div class="filtro">
        <fieldset>
            <legend>Selecione a Opção de Gerência:</legend>
            <a href="CadastraFuncionario.aspx">Cadastrar Funcionário</a><br />
            <a href="QuadroGeral.aspx">Quadro Geral</a><br />
        </fieldset>
    </div>
</asp:Content>

