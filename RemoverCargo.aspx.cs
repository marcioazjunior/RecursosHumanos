﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DeletarCargo : System.Web.UI.Page
{
    private Cargo[] cargos;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lstCargos_Init(object sender, EventArgs e)
    {
        cargos = Cargos.GetCargos();

        if (cargos == null)
            return;

        for (int i = 0; i < cargos.Length; ++i)
        {
            ListItem item = new ListItem();
            item.Text = cargos[i].DescCargo;
            lstCargos.Items.Add(item);
        }
    }

    protected void btnDeletar_Click(object sender, EventArgs e)
    {
        Cargo c = cargos[lstCargos.SelectedIndex];

        try
        {
            string query = "DELTE FROM CargoBeneficio WHERE idCargo=@idCargo";
            SqlCommand cmd = new SqlCommand(query, Conexao.IniciarConexao());
            cmd.Parameters.AddWithValue("@idCargo", c.IdCargo);
            cmd.ExecuteNonQuery();

            Cargos.RemoverCargo(c);
            Response.Redirect("RemoverCargo.aspx");
        }
        catch (Exception)
        {
            lblErro.Visible = true;
            lblErro.Text = "Não foi possível remover o cargo. Tenha certeza que nenhum funcionário possui o cargo selecionado.";
        }
    }
}