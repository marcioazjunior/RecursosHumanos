﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Comum.master" AutoEventWireup="true" CodeFile="RemoverCargo.aspx.cs" Inherits="DeletarCargo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="filtro">
        <asp:Label ID="lblErro" runat="server"></asp:Label>
        <label for="Deletar Cargo">Selecione o cargo a ser deletado: </label>
        <asp:DropDownList ID="lstCargos" runat="server" OnInit="lstCargos_Init"></asp:DropDownList>
        <asp:Button ID="btnDeletar" runat="server" Text="Deletar Cargo" OnClick="btnDeletar_Click" />
    </div>
</asp:Content>

